<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pemesanan;
use App\Models\Produk;
use Session;

class CheckoutController extends Controller
{
    public function checkout(Request $request)
    {
        $user_id = session()->get('user_id');

        $cartItems = Produk::where('id', session()->get('id_produk'))->get();
      
        foreach (session()->get('cart') as $item) {
            $pemesananItem = new Pemesanan;
            $pemesananItem->id_produk = $item['id_produk'] ?? null;
            $pemesananItem->id_konsumen = $user_id ?? null;
            $pemesananItem->jumlah_beli = $item['quantity'] ?? null;
            $pemesananItem->jumlah_harga = $item['harga_produk']*$item['quantity'];
            $pemesananItem->save();

            Session::flush();
        }

        return redirect()->route('home.index')->with('success', 'Pemesanan berhasil dilakukan');
    }
}
