<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Kategori;

class ProdukController extends Controller
{
    public function index(Request $request)
    {
        $data = Produk::where('status',0)
        ->where(function($query) use($request){
            if($request->get('nama_produk') != null){
                $query->whereDate('nama_produk', '>=', $request->get('nama_produk'));
            }
        })->get();
        
        return view('produk.index', compact('data'));
    }

    public function create()
    {
        $kategori = Kategori::get();

        return view('produk.create-update',compact('kategori'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_produk'      => 'required',
            'kategori_id'    => 'required',
            'harga_produk'    => 'required',
            'deskripsi'      => 'required',
        ]);

        if ($request->hasFile('foto_produk')) {
            $image = $request->file('foto_produk');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('foto_produk');
            $image->move($destinationPath, $image_name);
        }
     
        $data = New Produk();
        $data->nama_produk  = $request->input('nama_produk');
        $data->kategori_id  = $request->input('kategori_id');
        $data->harga_produk = $request->input('harga_produk');
        $data->deskripsi    = $request->input('deskripsi');
        $data->foto_produk  = $image_name;
        $data->save();

        return redirect(route('produk.index'))
                    ->with('success', 'Data berhasil disimpan');
    }

    public function edit($id)
    {
        $data       = Produk::findOrFail($id);
        $kategori   = Kategori::where('status',0)->get();
    
        return view('produk.create-update', compact('data','kategori'));
    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'nama_produk'      => 'required',
            'kategori_id'    => 'required',
            'harga_produk'    => 'required',
            'deskripsi'      => 'required',
        ]);

        if ($request->hasFile('foto_produk')) {
            $image = $request->file('foto_produk');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('foto_produk');
            $image->move($destinationPath, $image_name);
        }else{
            $produk = Produk::find($id);
            $image_name = $produk->nama_produk;
        }
            
        $data = Produk::find($id);
        $data->nama_produk  = $request->input('nama_produk');
        $data->kategori_id  = $request->input('kategori_id');
        $data->harga_produk = $request->input('harga_produk');
        $data->deskripsi    = $request->input('deskripsi');
        $data->foto_produk  = $image_name;
        $data->save();

        return redirect(route('produk.index'))
                    ->with('success', 'Data berhasil disimpan');
    }

    public function destroy($id)
    {
        $data           = Produk::findOrFail($id);
        $data->status   = 1;
        $data->save();

        return redirect(route('produk.index'))
                    ->with('success', 'Data berhasil dihapus');
    }
}
