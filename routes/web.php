<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PemesananController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KeranjangController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\KonsumenController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return redirect()->to('home');
});

//Admin

//produk
Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

//produk
Route::prefix('produk')->group(function () {
    Route::get('/', [ProdukController::class, 'index'])->name('produk.index');
    Route::get('/create', [ProdukController::class, 'create'])->name('produk.create');
    Route::post('/store', [ProdukController::class, 'store'])->name('produk.store');
    Route::get('/edit/{id}', [ProdukController::class, 'edit'])->name('produk.edit');
    Route::post('/update/{id}', [ProdukController::class, 'update'])->name('produk.update');
    Route::get('/destroy/{id}', [ProdukController::class, 'destroy'])->name('produk.destroy');
});

//pemesanan
Route::prefix('pemesanan')->group(function () {
    Route::get('/', [PemesananController::class, 'index'])->name('pemesanan.index');
});

//kategori
Route::prefix('kategori')->group(function () {
    Route::get('/', [KategoriController::class, 'index'])->name('kategori.index');
    Route::get('/create', [KategoriController::class, 'create'])->name('kategori.create');
    Route::post('/store', [KategoriController::class, 'store'])->name('kategori.store');
    Route::get('/edit/{id}', [KategoriController::class, 'edit'])->name('kategori.edit');
    Route::post('/update/{id}', [KategoriController::class, 'update'])->name('kategori.update');
    Route::get('/destroy/{id}', [KategoriController::class, 'destroy'])->name('kategori.destroy');
});

//konsumen
Route::prefix('konsumen')->group(function () {
    Route::get('/', [KonsumenController::class, 'index'])->name('konsumen.index');
});

//user
Route::prefix('home')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home.index');
});

Route::prefix('form')->group(function () {
    Route::get('/', [FormController::class, 'index'])->name('form');
    Route::post('/store', [FormController::class, 'store'])->name('form.store');
});

Route::prefix('keranjang')->group(function () {
    Route::get('/', [KeranjangController::class, 'index'])->name('keranjang');
    Route::get('/add-to-cart/{id}', [KeranjangController::class, 'cart'])->name('add-to-cart');
});

Route::prefix('checkout')->group(function () {
    Route::get('/', [CheckoutController::class, 'checkout'])->name('checkout');
});