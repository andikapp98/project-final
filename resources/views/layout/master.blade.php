<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Connect Plus</title>
    <link rel="stylesheet" href="{{asset('/template/assets/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('/template/assets/vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('/template/assets/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('/template/assets/vendors/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('/template/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('/template/assets/css/style.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.3.2/css/fixedHeader.dataTables.min.css">
    <link rel="shortcut icon" href="{{asset('/template/assets/images/favicon.png')}}" />
  </head>
  <body>
    <div class="container-scroller">
        @include('partials.navbar')
          <div class="container-fluid page-body-wrapper">
            @include('partials.sidebar')
            @yield('content')
            <footer class="footer">
              <div class="footer-inner-wraper">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                  <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © bootstrapdash.com 2020</span>
                  <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap dashboard templates</a> from Bootstrapdash.com</span>
                </div>
              </div>
            </footer>
        </div>
      </div>
    </div>
    
    <script src="{{asset('/template/assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{asset('/template/assets/vendors/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('/template/assets/vendors/jquery-circle-progress/js/circle-progress.min.js')}}"></script>
    <script src="{{asset('/template/assets/js/off-canvas.js')}}"></script>
    <script src="{{asset('/template/assets/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('/template/assets/js/misc.js')}}"></script>
    <!-- <script src="{{asset('/template/assets/js/dashboard.js')}}"></script> -->

    @stack('scripts')
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.3.2/js/dataTables.fixedHeader.min.js"></script>
  </body>
</html>