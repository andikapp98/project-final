@extends('layout.masterfe')

@section('content')
    <div class="popular-items">
        <div class="container">
            <!-- Section tittle -->
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-8 col-md-10">
                    <div class="section-tittle mb-70 text-center">
                        <h2>Popular Items</h2>
                        <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($data as $d)
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <div class="single-popular-items mb-50 text-center">
                        <div class="popular-img">
                            <img src="{{asset('/foto_produk/'.$d->foto_produk)}}" width="200px">
                            <div class="img-cap">
                                <span>
                                    <a href="{{ route('add-to-cart', $d->id) }}" class="btn btn-primary">Add to Cart</a>
                                </span>
                            </div>
                            <div class="favorit-items">
                                <span class="flaticon-heart"></span>
                            </div>
                        </div>
                        <div class="popular-caption">
                            <h3><a href="product_details.html">{{ $d->nama_produk }}</a></h3>
                            <span>{{number_format($d->harga_produk)}}</span>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <!-- Button -->
            <div class="row justify-content-center">
                <div class="room-btn pt-70">
                    <a href="catagori.html" class="btn view-btn1">View More Products</a>
                </div>
            </div>
        </div>
    </div>
@endsection